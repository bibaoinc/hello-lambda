package com.bibao.lambda.hello;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.lambda.runtime.Context;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class HelloLambdaHandlerTest {

    private static String input;

    @BeforeClass
    public static void createInput() throws IOException {
        input = "Alice";
    }

    private Context createContext() {
        TestContext ctx = new TestContext();
        ctx.setFunctionName("HelloLambdaHandler");

        return ctx;
    }

    @Test
    public void testHelloLambdaHandler() {
        HelloLambdaHandler handler = new HelloLambdaHandler();
        Context ctx = createContext();

        String output = handler.handleRequest(input, ctx);

        Assert.assertEquals("Hello Alice from Lambda!", output);
    }
}
