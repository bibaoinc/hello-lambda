package com.bibao.lambda.hello;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class HelloLambdaHandler implements RequestHandler<String, String> {

    @Override
    public String handleRequest(String name, Context context) {
        context.getLogger().log("Input name: " + name);

        return "Hello " + name + " from Lambda!";
    }

}
